<?php

namespace TeamBooking\Admin;
defined('ABSPATH') or die('No script kiddies please!');

/**
 * Class Misc
 *
 * @author VonStroheim
 */
class Misc
{
    /**
     * @var Misc
     */
    private static $_instance;

    /**
     * @return Misc
     */
    public static function render()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Generate the tab wrapper for admin page
     *
     * @param string $active_tab
     */
    public function getTabWrapper($active_tab)
    {
        $header = new Framework\Header();
        $header->setPluginData(TEAMBOOKING_FILE_PATH);
        $header->setMainText('Hi, ' . wp_get_current_user()->user_firstname);
        $header->addTab('overview', __('Overview', 'team-booking'), 'dashicons-chart-area', $active_tab === 'overview');
        $header->addTab('events', __('Services', 'team-booking'), 'dashicons-clipboard', $active_tab === 'events');
        $header->addTab('coworkers', __('Coworkers', 'team-booking'), 'dashicons-groups', $active_tab === 'coworkers');
        $header->addTab('customers', __('Customers', 'team-booking'), 'dashicons-id', $active_tab === 'customers');
        $header->addTab('personal', __('Personal', 'team-booking'), 'dashicons-businessman', $active_tab === 'personal');
        $header->addTab('aspect', __('Frontend style', 'team-booking'), 'dashicons-art', $active_tab === 'aspect');
        $header->addTab('general', __('Core settings', 'team-booking'), 'dashicons-admin-generic', $active_tab === 'general');
        $header->addTab('payments', __('Payment gateways', 'team-booking'), 'dashicons-cart', $active_tab === 'payments');
        $header->addTab('pricing', __('Promotions', 'team-booking'), 'dashicons-money', $active_tab === 'pricing');
        $header->render();
    }

    /**
     * Generate the tab wrapper for admin page (coworker)
     *
     * @param string $active_tab
     */
    public function getTabWrapperCoworker($active_tab)
    {
        $header = new Framework\Header();
        $header->setPluginData(TEAMBOOKING_FILE_PATH);
        $header->setMainText('Hi, ' . wp_get_current_user()->user_firstname);
        $header->addTab('overview', __('Overview', 'team-booking'), 'dashicons-chart-area', $active_tab === 'overview');
        $header->addTab('events', __('Services', 'team-booking'), 'dashicons-clipboard', $active_tab === 'events');
        $header->addTab('personal', __('Personal', 'team-booking'), 'dashicons-businessman', $active_tab === 'personal');
        $header->render();
    }

    public function getWhatsnewPage()
    {
        ?>
        <div class="wrap about-wrap">
        <h1><?= esc_html__('Welcome to TeamBooking 2.3', 'team-booking') ?></h1>
        <p class="about-text">
            <?= esc_html__('Thank you for updating to the latest version.', 'team-booking') ?>
        </p>
        <div class="wp-badge"
             style="    background: url(<?= TEAMBOOKING_URL . '/images/logo-white.png' ?>) center 45px no-repeat #0073aa;">
            Ver. <?= TEAMBOOKING_VERSION ?>
        </div>

        <div class="feature-section two-col">
            <h2><?= esc_html__('Frontend improvements', 'team-booking') ?></h2>
            <div class="col">
                <img src="<?= TEAMBOOKING_URL . '/images/feature-frontend-1.png' ?>" alt="Maps"
                     style="border: 5px solid lightgrey;box-sizing: border-box;">
                <h3><?= esc_html__('Say hello to upcoming events list!', 'team-booking') ?></h3>
                <p>
                    <?= esc_html__('A new shortcode allows you to display a customizable list of upcoming events! Handy, uh?', 'team-booking') ?>
                </p>
            </div>
            <div class="col">
                <div>
                    <img src="<?= TEAMBOOKING_URL . '/images/feature-frontend-2.png' ?>" alt="Descriptions"
                         style="border: 5px solid lightgrey;box-sizing: border-box;">
                </div>
                <h3><?= esc_html__('A brand new additional slot display style', 'team-booking') ?></h3>
                <p>
                    <?= esc_html__('You are able to choose a new slot display style globally or on a shortcode basis. And yes: more styles will come!', 'team-booking') ?>
                </p>
            </div>
        </div>

        <div class="feature-section three-col">
            <h2><?= esc_html__('Backend improvements', 'team-booking') ?></h2>
            <div class="col">
                <div>
                    <img src="<?= TEAMBOOKING_URL . '/images/feature-backend-1.png' ?>" alt="Visual Composer"
                         style="border: 5px solid lightgrey;box-sizing: border-box;">
                </div>
                <h3><?= esc_html__('Visual Composer elements.', 'team-booking') ?></h3>
                <p>
                    <?= esc_html__('The popular Page Builder is fully supported by TeamBooking. You can add all the shortcodes even via frontend live editor.', 'team-booking') ?>
                </p>
            </div>
            <div class="col">
                <div>
                    <img src="<?= TEAMBOOKING_URL . '/images/feature-backend-2.png' ?>" alt="Overview"
                         style="border: 5px solid lightgrey;box-sizing: border-box;">
                </div>
                <h3><?= esc_html__('New promotions features.', 'team-booking') ?></h3>
                <p>
                    <strong>
                        <?= esc_html__('Usage limit:', 'team-booking') ?>
                    </strong>
                    <?= esc_html__('you can set a limit of usages (one usage = one slot) for your campaigns or coupons. Once the limit is reached, the promotion will be stopped. If you want, you can raise the limit then.', 'team-booking') ?>
                </p>
                <p>
                    <strong>
                        <?= esc_html__('Time bounds:', 'team-booking') ?>
                    </strong>
                    <?= esc_html__('you can specify a minimum time (and/or a maximum time) which the promotion targets. If a timeslot does not comply with those bounds, the promotion will not be applied to it.', 'team-booking') ?>
                </p>
            </div>
            <div class="col">
                <img src="<?= TEAMBOOKING_URL . '/images/feature-backend-3.png' ?>" alt="Allowed Services"
                     style="border: 5px solid lightgrey;box-sizing: border-box;">
                <h3><?= esc_html__('Dynamic booked slot titles.', 'team-booking') ?></h3>
                <p>
                    <?= esc_html__("For your Appointment services, now you can set a dynamic booked event title. That means you can add customer's data (e-mail and name) directly to the booked slot title in Google Calendar, to check who reserved them without even opening the slots.", 'team-booking') ?>
                </p>
            </div>
        </div>

        <div class="changelog">
            <h2><?= esc_html__('Under the hood', 'team-booking') ?></h2>

            <div class="under-the-hood three-col">
                <div class="col">
                    <h3><?= esc_html__('PayPal tweaks', 'team-booking') ?></h3>
                    <p>
                        <?= esc_html__('You can set a logo to show in the checkout page. PayPal primary e-mail address (if needed) can also be set now.', 'team-booking') ?>
                    </p>
                </div>
                <div class="col">
                    <h3><?= esc_html__('Tabbed modals', 'team-booking') ?></h3>
                    <p>
                        <?= esc_html__('Some data modals in the back-end are now tabbed, for your convenience.', 'team-booking') ?>
                    </p>
                </div>
                <div class="col">
                    <h3><?= esc_html__('Bugfixes', 'team-booking') ?></h3>
                    <p>
                        <?= esc_html__('Some annoying bugs were fixed, but please contact our support as soon as you will find a new one!', 'team-booking') ?>
                    </p>
                </div>
            </div>
        </div>

        <?php
    }

}
