<?php

namespace TeamBooking\Database;

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

use TeamBooking\Functions,
    TeamBooking\Toolkit;

class Events
{
    /**
     * @param      $param
     * @param      $value
     * @param int  $min_time
     * @param null $max_time
     *
     * @return array
     */
    private static function getBy($param, $value, $min_time = 0, $max_time = NULL, $limit = 0)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";
        $limit = !$limit ? '' : ' ORDER BY event_start LIMIT ' . $limit;
        if (is_null($max_time)) {
            $prepared_query = $wpdb->prepare("
                SELECT coworker_id, calendar_id, event_id, location, summary, event_start, event_end, organizer, allday
                FROM $table_name WHERE $param = %s AND event_end > %d
                " . $limit, array($value, $min_time));
        } else {
            $prepared_query = $wpdb->prepare("
                SELECT coworker_id, calendar_id, event_id, location, summary, event_start, event_end, organizer, allday
                FROM $table_name WHERE $param = %s AND event_end > %d AND event_start < %d
                " . $limit, array($value, $min_time, $max_time));
        }
        $results = $wpdb->get_results($prepared_query);
        $return = array();
        foreach ($results as $result) {
            $return[ $result->coworker_id ][ $result->calendar_id ][ $result->event_id ] = Events::map($result);
        }

        return $return;
    }

    public static function getAll()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";
        $results = $wpdb->get_results("
			SELECT coworker_id, calendar_id, event_id
			FROM $table_name
		");
        $return = array();
        foreach ($results as $result) {
            $return[ $result->coworker_id ][ $result->calendar_id ][ $result->event_id ] = TRUE;
        }

        return $return;
    }

    public static function getByCalendar($calendar_id)
    {
        return Events::getBy('calendar_id', $calendar_id);
    }

    public static function getByCoworker($coworker_id, $min_time = 0, $max_time = NULL, $limit = 0)
    {
        $events = Events::getBy('coworker_id', $coworker_id, $min_time, $max_time, $limit);

        if (isset($events[ $coworker_id ])) {
            return $events[ $coworker_id ];
        } else {
            return array();
        }
    }

    /**
     * @param \Google_Service_Calendar_Event[] $data
     * @param  string                          $calendar_id
     *
     * @return mixed
     */
    public static function insert(array $data, $calendar_id)
    {
        $coworkers = Functions\getAuthCoworkersList();
        $calendars = array();
        foreach ($coworkers as $coworker_id => $coworker_data) {
            foreach ($coworker_data['calendars'] as $calendar) {
                $calendars[ $calendar['calendar_id'] ] = $coworker_id;
            }
        }
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";
        $query = "INSERT INTO $table_name (
                coworker_id, calendar_id, event_id, 
                updated, created, color_id, description, 
                hangout_link, html_link, location, 
                recurrence, recurring_event_id, 
                summary, event_start, event_end, 
                organizer, guests, allday
            ) 
            VALUES ";
        $values = array();
        foreach ($data as $item) {
            if ($item->getStart()->getDateTime()) {
                $start = $item->getStart()->getDateTime();
                $end = $item->getEnd()->getDateTime();
                $allday = 0;
            } else {
                $start = $item->getStart()->getDate();
                $end = $item->getEnd()->getDate();
                $allday = 1;
            }
            $values[] = $wpdb->prepare(
                "(%d,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%d)",
                $calendars[ $calendar_id ],
                $calendar_id,
                $item->getId(),
                $item->getUpdated(),
                $item->getCreated(),
                $item->getColorId(),
                $item->getDescription(),
                $item->getHangoutLink(),
                $item->getHtmlLink(),
                $item->getLocation(),
                $item->getRecurrence(),
                $item->getRecurringEventId(),
                is_null($item->getSummary()) ? '' : $item->getSummary(),
                strtotime($start),
                strtotime($end),
                $item->getOrganizer()->getEmail(),
                serialize($item->getAttendees()),
                $allday
            );
        }
        $query .= implode(", ", $values);

        return $wpdb->query($query);
    }

    /**
     * @param \Google_Service_Calendar_Event[] $data
     *
     * @return int
     */
    public static function update(array $data)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";
        $i = 0;
        foreach ($data as $item) {
            if ($item->getStart()->getDateTime()) {
                $start = $item->getStart()->getDateTime();
                $end = $item->getEnd()->getDateTime();
                $allday = 0;
            } else {
                $start = $item->getStart()->getDate();
                $end = $item->getEnd()->getDate();
                $allday = 1;
            }
            $i += $wpdb->update($table_name,
                array(
                    'updated'            => $item->getUpdated(),
                    'color_id'           => $item->getColorId(),
                    'description'        => $item->getDescription(),
                    'hangout_link'       => $item->getHangoutLink(),
                    'html_link'          => $item->getHtmlLink(),
                    'location'           => $item->getLocation(),
                    'recurrence'         => $item->getRecurrence(),
                    'recurring_event_id' => $item->getRecurringEventId(),
                    'summary'            => is_null($item->getSummary()) ? '' : $item->getSummary(),
                    'event_start'        => strtotime($start),
                    'event_end'          => strtotime($end),
                    'organizer'          => $item->getOrganizer()->getEmail(),
                    'guests'             => serialize($item->getAttendees()),
                    'allday'             => $allday
                ),
                array('event_id' => $item->getId(), 'calendar_id' => $item->getOrganizer()->getEmail())
            );
        }

        return $i;
    }

    /**
     * @param $event_id
     * @param $calendar_id
     *
     * @return mixed
     */
    public static function removeEvent($event_id, $calendar_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";

        return $wpdb->delete($table_name, array('event_id' => $event_id, 'calendar_id' => $calendar_id));
    }

    public static function removeCalendar($calendar_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";

        return $wpdb->delete($table_name, array('calendar_id' => $calendar_id));
    }

    public static function removeCoworker($coworker_id)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";

        return $wpdb->delete($table_name, array('coworker_id' => $coworker_id));
    }

    public static function reset()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_events";
        $wpdb->query("TRUNCATE TABLE $table_name");

        return;
    }

    /**
     * @param \stdClass $db_object
     *
     * @return eventObject
     */
    private static function map(\stdClass $db_object)
    {
        $event = new eventObject();
        $event->allday = (bool)$db_object->allday;
        $event->id = $db_object->event_id;
        $event->summary = $db_object->summary;
        $event->location = $db_object->location;
        $event->organizer = $db_object->organizer;
        $date_obj = \DateTime::createFromFormat('U', $db_object->event_start);
        $event->start = $event->allday ? $date_obj->format('Y-m-d') : $date_obj->format(\DateTime::RFC3339);
        $date_obj = \DateTime::createFromFormat('U', $db_object->event_end);
        $event->end = $event->allday ? $date_obj->format('Y-m-d') : $date_obj->format(\DateTime::RFC3339);

        return $event;
    }
}

class eventObject
{
    public $id;
    public $summary;
    public $location;
    public $organizer;
    public $start;
    public $end;
    public $allday;
}