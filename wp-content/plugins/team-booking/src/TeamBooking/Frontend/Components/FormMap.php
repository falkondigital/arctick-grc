<?php

namespace TeamBooking\Frontend\Components;
defined('ABSPATH') or die('No script kiddies please!');

class FormMap
{
    /**
     * @param string $location
     */
    public static function get($location)
    {
        ob_start();
        FormMap::render($location);

        return ob_get_clean();
    }

    /**
     * @param string $location
     */
    public static function render($location)
    {
        ?>
        <div class="ui horizontal tbk-divider tbk-map" style="display:none">
            <i class="marker tb-icon"></i>
        </div>
        <div class="tbk-map tbk-address" style="display:none">
            <?= $location ?>
            <a class="tbk-get-directions" target="_blank"
               href="//maps.google.com?daddr=<?= $location ?>">
                (<?= esc_html__('get directions', 'team-booking') ?>)
            </a>
        </div>
        <div class="tbk-segment tbk-map tbk-map-canvas"
             data-zoom="<?= \TeamBooking\Functions\getSettings()->getGmapsZoomLevel() ?>"
             style="display:none"></div>
        <?php
    }
}