<?php

namespace TeamBooking\PaymentGateways\PayPal;
defined('ABSPATH') or die('No script kiddies please!');
use TeamBooking\Functions;

/**
 * Class Gateway
 *
 * @author VonStroheim
 */
class Gateway implements \TeamBooking\PaymentGateways\Gateway
{
    private $currency;
    private $quantity;
    private $price;
    private $id;
    private $item_name;
    private $redirect_url;

    public function __construct()
    {
    }

    /**
     * @param string $code
     */
    public function setCurrency($code)
    {
        $this->currency = $code;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @param $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param string $name
     */
    public function setItemName($name)
    {
        $this->item_name = $name;
    }

    /**
     * @param $id
     */
    public function setIpnId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $url
     */
    public function setRedirectUrl($url)
    {
        $this->redirect_url = $url;
    }

    /**
     * @return string
     */
    public function processPayment()
    {
        // Prepare GET data
        $query = array();
        #$query['notify_url'] = 'http://XXXXXXX.ngrok.io/wp-admin/' . 'admin-ajax.php?action=teambooking_ipn_listener&paypal=1';
        $query['notify_url'] = admin_url() . 'admin-ajax.php?action=teambooking_ipn_listener&paypal=1';
        $query['cmd'] = '_xclick';
        $query['cbt'] = 'Return to ' . get_bloginfo('name');
        $query['currency_code'] = $this->currency;
        $query['business'] = Functions\getSettings()->getPaymentGatewaySettingObject('paypal')->getAccountEmail();
        $image_url = wp_get_attachment_image_src(Functions\getSettings()->getPaymentGatewaySettingObject('paypal')->getLogoMediaId());
        if (is_array($image_url)) {
            $query['image_url'] = $image_url[0];
        }
        $query['item_name'] = $this->item_name;
        $query['quantity'] = $this->quantity;
        $query['amount'] = $this->price;
        $query['custom'] = $this->id;
        $query['return'] = $this->redirect_url;
        $query['charset'] = 'utf-8';
        $query['lc'] = $this->getCountryCode();

        // Prepare query string
        $query_string = http_build_query($query);

        // Return
        return $this->getPayPalUrl() . $query_string;
    }

    /**
     * @return string
     */
    private function getPayPalUrl()
    {
        if (Functions\getSettings()->getPaymentGatewaySettingObject('paypal')->isUseSandbox()) {
            return 'https://www.sandbox.paypal.com/cgi-bin/webscr?';
        } else {
            return 'https://www.paypal.com/cgi-bin/webscr?';
        }
    }

    /**
     * @return string
     */
    private function getCountryCode()
    {
        $pp_locales = array(
            'AU',
            'AT',
            'BE',
            'BR',
            'CA',
            'CH',
            'CN',
            'DE',
            'ES',
            'GB',
            'FR',
            'IT',
            'NL',
            'PL',
            'PT',
            'RU',
            'US',
            'da_DK',
            'he_IL',
            'id_ID',
            'ja_JP',
            'no_NO',
            'pt_BR',
            'ru_RU',
            'sv_SE',
            'th_TH',
            'tr_TR',
            'zh_CN',
            'zh_HK',
            'zh_TW',
        );
        $wp_locale = get_locale();
        if (in_array(get_locale(), $pp_locales)) {
            return $wp_locale;
        } else {
            $wp_cc = substr($wp_locale, -2);
            if (in_array($wp_cc, $pp_locales)) {
                return $wp_cc;
            } else {
                return 'US';
            }
        }
    }

}
