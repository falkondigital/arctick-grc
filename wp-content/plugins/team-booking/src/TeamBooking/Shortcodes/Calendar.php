<?php

namespace TeamBooking\Shortcodes;
defined('ABSPATH') or die('No script kiddies please!');
use TeamBooking\Database\Services,
    TeamBooking\Toolkit,
    TeamBooking\Functions;

/**
 * Class Calendar
 *
 * @author VonStroheim
 */
class Calendar
{
    /**
     * TeamBooking Calendar Shortcode
     *
     * @param $atts
     *
     * @return string
     * @throws \Exception
     */
    public static function render($atts)
    {
        extract(shortcode_atts(array(
            'booking'     => NULL,
            'coworker'    => NULL,
            'read_only'   => FALSE,
            'logged_only' => FALSE,
            'nofilter'    => FALSE, // Hide filtering buttons
            'notimezone'  => FALSE, // Hide timezone selector
            'slot_style'  => Functions\getSettings()->getSlotStyle(),
        ), $atts, 'tb-calendar'));

        if (!defined('TBK_CALENDAR_SHORTCODE_FOUND')) {
            define('TBK_CALENDAR_SHORTCODE_FOUND', TRUE);
        }

        if (!wp_script_is('tb-frontend-script', 'registered')) {
            Functions\registerFrontendResources();
        }

        Functions\enqueueFrontendResources();

        // Read-only mode is identified by lenght of istance id
        $unique_id = !$read_only ? Toolkit\randomNumber(8) : Toolkit\randomNumber(6);

        if (!$logged_only || ($logged_only && is_user_logged_in())) {
            if (NULL !== $booking && !empty($booking)) {
                $services = array_map('trim', explode(',', $booking));
                foreach ($services as $key => $booking) {
                    try {
                        // Remove inactive services
                        if (!Services::get($booking)->isActive()) {
                            unset($services[ $key ]);
                        }
                    } catch (\Exception $exc) {
                        unset($services[ $key ]);
                        continue;
                    }
                }
                if (empty($services)) {
                    return esc_html__('WARNING: service ID(s) not found. Please check the shortcode syntax and ensure at least one of the specified services is active.', 'team-booking');
                }
            } else {
                // Service(s) not specified, picking all of them
                $services = Functions\getSettings()->getServiceIdList();
                // Remove inactive services
                foreach ($services as $key => $service) {
                    if (!Services::get($service)->isActive()) {
                        unset($services[ $key ]);
                    }
                }
                if (count($services) <= 0) {
                    // Service(s) not specified, but no service available
                    return esc_html__('No active services', 'team-booking');
                }
            }
            $coworkers = NULL !== ($coworker) ? array_map('trim', explode(',', $coworker)) : array();
            $calendar = new \TeamBooking\Calendar();
            $parameters = new \TeamBooking\RenderParameters();
            $parameters->setServiceIds($services);
            $parameters->setRequestedServiceIds($services);
            $parameters->setCoworkerIds($coworkers);
            $parameters->setRequestedCoworkerIds($coworkers);
            $parameters->setInstance($unique_id);
            $parameters->setTimezone(Toolkit\getTimezone());
            $parameters->setIsAjaxCall(FALSE);
            $parameters->setNoFilter($nofilter);
            $parameters->setNoTimezone($notimezone);
            $parameters->setAltSlotStyle($slot_style);
            ob_start();
            ?>
            <div class="ui calendar_main_container" id="tbk-container-<?= $unique_id ?>" aria-live="polite"
                 data-postid="<?= get_the_ID() ?>">
                <?php
                $calendar->getCalendar($parameters);
                ?>
            </div>
            <script>
                if (typeof tbkLoadInstance === "function") {
                    tbkLoadInstance(jQuery('#tbk-container-<?= $unique_id ?>'));
                }
            </script>
            <?php
            return ob_get_clean();
        }
    }
}