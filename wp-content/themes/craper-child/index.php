<?php get_header();


if( $wp_query->is_posts_page )
{

	$quereid_object = get_queried_object();

	$meta = get_post_meta($quereid_object->ID, '_craper_page_meta', true);
	$sidebar = sh_set( $meta, 'page_sidebar' );
	$layout = sh_set($meta, 'layout');
}else {
	$theme_options = array();//_WSH()->option();
	$sidebar = sh_set( $theme_options, 'blog_sidebar', 'default-sidebar' );
	$layout = sh_set($theme_options, 'blog_layout', 'right');
}


?>
<!---->
<!--<article class="banner">-->
<!--	<div class="container">-->
<!--		<h2>--><?php //_e( 'Blog Archives', SH_NAME ); ?><!--</h2>-->
<!--		<div class="breadcrumbs">-->
<!--			<ul class="breadcrumb">sdfa-->
<!--				--><?php //echo get_the_breadcrumb(); ?>
<!--			</ul>-->
<!--		</div>-->
<!--	</div>-->
<!--</article>-->

<?php echo do_shortcode('[rev_slider alias="blog-banner"]')?>

<h1 style="text-align: center; padding-top:50px;"><span style="color: #27aae1;">Latest&nbsp;News</span></h1>

<article role="main">
	<div class="section">
		<div class="container">
			<div class="row">

				<?php $class = ( $layout == 'full' ) ? 'col-md-12' : 'col-xs-12 col-sm-8 col-md-8';
					if( $layout == 'left' ): ?>
						<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
                    		<?php dynamic_sidebar( $sidebar ); ?>
                		</div>
            	<?php endif; ?>


				<div class="<?php echo $class; ?>">

					<?php if( $layout == 'full' ): ?>
					<div class="blog-centered">
					<?php endif; ?>

					<div class="contents">

						<?php get_template_part('content', 'blog'); ?>
						<?php _the_pagination(); ?>

					</div>

					<?php if( $layout == 'full' ): ?>
					</div>
					<?php endif; ?>

				</div>

				<?php if( $layout == 'right' ): ?>

                    <div class="col-xs-12 col-sm-4 col-md-4 sidebar falkon-blog-sb">
                    	<?php dynamic_sidebar( $sidebar ); ?>
                	</div>

				<?php endif; ?>

			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
