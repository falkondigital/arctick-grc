<?php
	if(get_the_ID() != 6223){
?>
<footer id="footertop">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-4 col-md-4">
          <h3>OUR LOCATIONS</h3>
          <p><a target="_blank" href="https://www.google.co.uk/maps/place/Sale+M33+7BU/@53.4320076,-2.3197338,17z/data=!3m1!4b1!4m5!3m4!1s0x487bac4a68eb729f:0x91f730091ce0a0af!8m2!3d53.4318861!4d-2.3174023">Manchester</a><br />
            <a target="_blank" href="https://www.google.co.uk/maps/place/Ruddington,+Nottingham+NG11+6JS/@52.8816172,-1.1499248,16z/data=!3m1!4b1!4m5!3m4!1s0x4879c326fd866609:0xf6659e408dd09682!8m2!3d52.8825237!4d-1.1433252" >Nottingham</a><br />
            <a target="_blank" href="https://www.google.co.uk/maps/place/Charterhouse+St,+London+EC1M+6HR/@51.5197396,-0.1034548,17z/data=!3m1!4b1!4m5!3m4!1s0x48761b53de1fdc97:0xdc2000fc8b74d4f2!8m2!3d51.5200217!4d-0.1013187">London</a></p>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-4">
          <h3>WORKING IN PARTNERSHIP WITH</h3>
          <img style="width:50%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2017/01/Nationwide-BS-Logo-sRGB.png" title="Nationwide Building Society" alt="Nationwide Building Society logo"/>
        </div>
        <div id="sharebox" class="col-xs-12 col-sm-4 col-md-4 share-ticker">
<!--          <h3>GROUP SHARE PRICE</h3>-->
<!--          <iframe class="share-iframe" frameborder=0 src="http://ir.tools.investis.com/Clients/uk/styles_and_wood1/Ticker/Ticker.aspx?culture=en-GB"></iframe>-->
        </div>
        <?php //dynamic_sidebar('footer-sidebar'); ?>

      </div>
        <div class="row">
          <div class="col-xs-6 col-sm-4 col-md-4">
            <h3>USEFUL LINKS</h3>
            <p><?php /*
              <a href="/sitemap/">Sitemap</a><br />*/ ?>
              <a href="/contact-us/">Contact Us</a><br>
                <a href="https://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Modern-Slavery-Statement.pdf" target="_blank">Modern Slavery Statement </a><br>
                <a href="https://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Health-Safety.pdf" target="_blank">Health & Safety Policy </a><br>
                <a href="https://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Quality-Policy.pdf" target="_blank">Quality Policy </a><br>
                <a href="https://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Environmental-Policy.pdf" target="_blank">Environmental Policy</a><br>
                <a href="https://www.stylesandwood-group.co.uk/wp-content/uploads/2017/07/Statement-of-Ethics.pdf">Statement of Ethics</a><br>
                <a href="https://www.arctick-grc.co.uk/cookie-policy/" target="">Cookie Policy</a><br>
                <a href="https://www.arctick-grc.co.uk/privacy-policy/" target="">Privacy Policy</a><br>
                <a href="https://www.arctick-grc.co.uk/terms-of-use/" target="">Terms of Use</a>
            </p>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4">
            <h3>ASSOCIATE MEMBERS OF</h3>
            <div class="row" style="min-height:110px;">
                <div class="col-xs-3 col-sm-6 col-md-3 text-center">
                    <a href="https://www.bba.org.uk" target="_blank"><img style="width:90%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2017/03/BBA-white-footer-logo.png" title="BBA - Voice of Banking" alt="BBA - Voice of Banking logo"/></a>
                </div>
                <div class="col-xs-3 col-sm-6 col-md-3  text-center">
                    <a href="https://www.fla.org.uk" target="_blank"><img style="width:90%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2017/09/fla-logo-footer.png" title="FLA" alt="FLA logo"/></a>
                </div>
                <div class="col-xs-3 col-sm-6 col-md-3 text-center">
                    <a href="https://www.ukfinance.org.uk" target="_blank"><img style="width:90%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2017/09/uk-finance-1.png" title="UK finance"/></a>
                </div>
                <div class="col-xs-3 col-sm-6 col-md-3 text-center">
                    <a href="https://www.theirm.org" target="_blank"><img style="width:90%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2017/09/irm.png" title="IRM" alt="IRM"/></a>
                </div>
            </div>
              <div class="row" style="min-height:110px;">
                  <div class="col-xs-3 col-sm-6 col-md-3 text-center">
                      <a href="https://www.csa-uk.com/" target="_blank"><img style="width:90%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2018/03/csa_supplier_membership_cmyk.png" title="CSA - CREDIT SERVICES ASSOCIATION" alt="CSA - CREDIT SERVICES ASSOCIATION logo"/></a>
                  </div>
                  <div class="col-xs-3 col-sm-6 col-md-4 text-center">
                      <a href="https://www.pimfa.co.uk/" target="_blank"><img style="width:90%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2018/05/PIMFA-logo.gif" title="PIMA - Building personal financial futures" alt="PIMA - Building personal financial futures logo"/></a>
                  </div>
              </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 text-right"><h3><a href="https://www.stylesandwood-group.co.uk"><img style="width:100%;height:auto;" src="https://www.arctick-grc.co.uk/wp-content/uploads/2018/05/Copyright-Logo-np-2018.png" title="Part of Styles & Wood Group &copy;<?php echo date('Y');?>" alt="Part of Styles & Wood Group &copy;<?php echo date('Y');?>"/></a></h3>
          </div>
      </div>
    </div>
  </footer>	<?php } ?>
</div>
<!-- /pageWrapper -->

<?php wp_footer(); ?>

<!-- Don't forget analytics -->

<script type="text/javascript">
_linkedin_data_partner_id = "42591";
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>



</body>

</html>
