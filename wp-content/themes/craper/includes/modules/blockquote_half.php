<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php if( have_posts()):?>
<div class="col-xs-12">
	<div class="qouts-box caro-1-col">
		<blockquote>
			<ul class="caro-slider-ul">
				
				<?php while( have_posts()): the_post();
							$meta = get_post_meta( get_the_id(), '_sh_testimonial_meta', true );//printr($meta);?>

				<li>
					<p><?php echo $this->excerpt(get_the_excerpt(), 200); ?></p>
					<h5><?php the_title();?></h5>
					<p><?php echo sh_set($meta, 'designation');?> - <?php echo sh_set($meta, 'company');?></p>
				</li>
				
				<?php endwhile; ?>
				
			</ul>
			<div class="caro-controls controls-right"> <a href="#" class="caro-prev"></a> <a href="#" class="caro-next"></a> </div>
		</blockquote>
	</div>
	<!-- /qouts-blox --> 
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>