<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php $delay = 0;?>
<?php if( have_posts()):?>
<div class="section"> 
	
	<!-- portfolio-area -->
	<div class="portfolio-area">
		<header class="heading">
			<h2><?php echo $title;?></h2>
		</header>
		<div class="container">
			<div class="portfolio-list col-3 margin-btm-50">
				<ul>
					
					<?php while( have_posts()): the_post();?>
					<?php
						$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );
						//$url = $thumb['0'];
					?>
				
					<li class="animated out" data-animation="fadeIn" data-delay="<?php echo $delay;?>">
						<div class="portfolio-box">
							<figure> <a href="<?php echo $url;?>" data-rel="prettyPhoto" title="<?php the_title();?>"> <?php the_post_thumbnail('370x300');?></a>
								<figcaption>
									<h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
									<p><?php echo get_the_term_list(get_the_id(), 'portfolio_category', '', ', '); ?></p>
								</figcaption>
							</figure>
						</div>
					</li>
					
					<?php $delay+=200; endwhile; ?>
					
				</ul>
			</div>
			<!-- /portfolio-list -->
			
			<div class="align-center"> <a href="<?php echo $button_link;?>" class="btn btn-light-dark"><?php echo $button_text;?></a> </div>
		</div>
	</div>
	<!-- /portfolio-area --> 
	
</div>

<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>