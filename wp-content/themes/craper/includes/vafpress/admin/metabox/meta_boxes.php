<?php
$options = array();
$options[] =  array(
	'id'          => '_craper_post_settings',
	'types'       => array('post'),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					array(
						'type' => 'radioimage',
						'name' => 'layout',
						'label' => __('Listing Layout', SH_NAME),
						'description' => __('Choose the layout for your page', SH_NAME),
						'items' => array(
							
							array(
								'value' => 'full',
								'label' => __('Full Width', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
							),
							array(
								'value' => 'left',
								'label' => __('Left Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
							),
							array(
								'value' => 'right',
								'label' => __('Right Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
							),
						),
					),
					array(
						'type' => 'select',
						'name' => 'post_sidebar',
						'label' => __('Sidebar', SH_NAME),
						'description' => __('Choose an sidebar for this deal', SH_NAME),
						'default' => '',
						'items' => sh_get_sidebars(true)	
					),
			
				),
);

/* Page options */
$options[] =  array(
	'id'          => '_craper_page_meta',
	'types'       => array('page'),
	'title'       => __('Page Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					array(
						'type' => 'radioimage',
						'name' => 'layout',
						'label' => __('Listing Layout', SH_NAME),
						'description' => __('Choose the layout for your page', SH_NAME),
						'items' => array(
							
							array(
								'value' => 'full',
								'label' => __('Full Width', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
							),
							array(
								'value' => 'left',
								'label' => __('Left Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
							),
							array(
								'value' => 'right',
								'label' => __('Right Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
							),
						),
					),
					array(
						'type' => 'select',
						'name' => 'page_sidebar',
						'label' => __('Sidebar', SH_NAME),
						'description' => __('Choose an sidebar for this deal', SH_NAME),
						'default' => '',
						'items' => sh_get_sidebars(true)	
					),
					
				),
);
/*Testimonial meta*/
$options[] =  array(
	'id'          => '_sh_testimonial_meta',
	'types'       => array('sh_testimonial'),
	'title'       => __('Testimonial Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
	
			array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'description' => __('Enter the designation of the person', SH_NAME),
					'default' => '',
				),
			array(
					'type' => 'textbox',
					'name' => 'company',
					'label' => __('Company', SH_NAME),
					'description' => __('Enter the Company name of person', SH_NAME),
					'default' => '',
				),
		),
);

/** Services meta */
$options[] =  array(
	'id'          => '_sh_services_meta',
	'types'       => array('sh_service'),
	'title'       => __('Service Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
			
			array(
					'type' => 'fontawesome',
					'name' => 'font_awesome_icon',
					'label' => __('Choos Font Awesome Icon', SH_NAME),
					'description' => __('Choose an icon from the font icons list', SH_NAME),
					'default' => '',
				),
			array(
					'type'  => 'slider',
					'name'  => 'skill_percent',
					'label' => __('Skill Level', SH_NAME),
					'min'   => 1,
					'max'   => 100,
				),
			array(
					'type'      => 'group',
					'repeating' => true,
					'sortable'  => true,
					'length'    => 1,
					'name'      => 'feature_group',
					'title'     => __('Features', SH_NAME),
					
					'fields'    => 
					array(

						array(
							'type' => 'textbox',
							'name' => 'feature',
							'label' => __('Feature', SH_NAME),
							'default' => '',
						),
					),
				),
	),
);


/** Team meta*/
$options[] =  array(
	'id'          => '_sh_team_meta',
	'types'       => array('sh_team'),
	'title'       => __('Team Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
			   array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'description' => __('Enter the designation of the member', SH_NAME),
					'default' => '',
				),
			   array(
					'type'      => 'group',
					'repeating' => true,
					'sortable'  => true,
					'length'    => 1,
					'name'      => 'social_icon_group',
					'title'     => __('Social icons', SH_NAME),
					
					'fields'    => 
					array(

						array(
							'type' => 'textbox',
							'name' => 'title',
							'label' => __('Title', SH_NAME),
							'default' => '',
						),
						array(
							'type' => 'textbox',
							'name' => 'social_url',
							'label' => __('URL', SH_NAME),
							'default' => '',
						),
						array(
								'type' => 'select',
								'name' => 'social_icon',
								'label' => __('Social icon', SH_NAME),
								'description' => __('Choose the social icon from the list', SH_NAME),
								'default' => '',
								'items' => array(
									'data' => array(
												array(
           												'source' => 'function',
           												'value' => 'vp_get_social_medias',
         											   ),
         											),
        										 ),
						),
						
					),
				),
				array(
					'type' => 'textbox',
					'name' => 'about_title',
					'label' => __('About Title', SH_NAME),
					'description' => __('Enter the title for about me page', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'about_subtitle',
					'label' => __('About Sub-Title', SH_NAME),
					'description' => __('Enter the sub-title for about me page', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textarea',
					'name' => 'about_text',
					'label' => __('About Text', SH_NAME),
					'description' => __('Enter the text for about me page', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textarea',
					'name' => 'skill_text',
					'label' => __('Skills Text', SH_NAME),
					'description' => __('Enter the text for skills section of page', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'hireme_link',
					'label' => __('Hire me Link', SH_NAME),
					'description' => __('Enter Link for Hire me button', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'site_url',
					'label' => __('Website url', SH_NAME),
					'description' => __('Enter site url', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'theme_url',
					'label' => __('Theme url', SH_NAME),
					'description' => __('Enter theme url', SH_NAME),
					'default' => '',
				),
				array(
					'type'      => 'group',
					'repeating' => true,
					'sortable'  => true,
					'length'    => 1,
					'name'      => 'skills_group',
					'title'     => __('Skills', SH_NAME),
					
					'fields'    => 
					array(

						array(
							'type' => 'textbox',
							'name' => 'title',
							'label' => __('Title', SH_NAME),
							'default' => '',
						),
						array(
							'type'  => 'slider',
							'name'  => 'skill_percent',
							'label' => __('Skill Level', SH_NAME),
							'min'   => 1,
							'max'   => 100,
						),
						
					),
				),
				
	),
);



/** Portfolio meta*/
$options[] =  array(
	'id'          => '_sh_portfolio_meta',
	'types'       => array('sh_portfolio'),
	'title'       => __('Portfolio Settings', SH_NAME),
	'priority'    => 'high',
	'template'    =>
				array(
					array(
						'type' => 'textbox',
						'name' => 'client_name',
						'label' => __('Client Name', SH_NAME),
						'description' => __('Enter the Client name', SH_NAME),
						'default' => '',
					),
					array(
						'type' => 'textbox',
						'name' => 'project_url',
						'label' => __('Project URL', SH_NAME),
						'description' => __('Enter the project url', SH_NAME),
						'default' => '',
					),
					array(
						'type'      => 'group',
						'repeating' => true,
						'sortable'  => true,
						'length'    => 1,
						'name'      => 'feature_group',
						'title'     => __('Features', SH_NAME),
						
						'fields'    => 
						array(

							array(
								'type' => 'textbox',
								'name' => 'feature',
								'label' => __('Feature', SH_NAME),
								'default' => '',
							),
						),
					),
					array(
						'type' => 'radioimage',
						'name' => 'layout',
						'label' => __('Listing Layout', SH_NAME),
						'description' => __('Choose the layout for your page', SH_NAME),
						'items' => array(
							
							array(
								'value' => 'full',
								'label' => __('Full Width', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/1col.png',
							),
							array(
								'value' => 'left',
								'label' => __('Left Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cl.png',
							),
							array(
								'value' => 'right',
								'label' => __('Right Sidebar', SH_NAME),
								'img' => get_template_directory_uri().'/includes/vafpress/public/img/2cr.png',
							),
						),
					),
					array(
						'type' => 'select',
						'name' => 'page_sidebar',
						'label' => __('Sidebar', SH_NAME),
						'description' => __('Choose an sidebar for this deal', SH_NAME),
						'default' => '',
						'items' => sh_get_sidebars(true)	
					),
					array(
						'type' => 'toggle',
						'name' => 'is_related_post',
						'label' => __('Show related post ?', SH_NAME),
						'default' => '',
						'description' => __('Choose whether to show related posts or not', SH_NAME)
					),
				), 
);



/** Partner Options Options*/
/*$options[] =  array(
	'id'          => 'sh_partner_meta',
	'types'       => array('sh_partner'),
	'title'       => __('Partner Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
						
			array(
				'type' => 'textbox',
				'name' => 'link',
				'label' => __('Partner URL', SH_NAME),
				'default' => '',
			),
	),
);*/

/*$options[] =  array(
	'id'          => 'sh_services_option',
	'types'       => array( 'sh_service' ),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 

			array(
				
				array(
					'type' => 'fontawesome',
					'name' => 'fontawesome',
					'label' => __('Service Icon', SH_NAME),
					'default' => '',
				),
				array(
					'type'      => 'group',
					'repeating' => true,
					'sortable'  => true,
					'name'      => 'row',
					'title'     => __('Row', 'vp_textdomain'),
					'fields'    => array(
						
						array(
							'type'  => 'slider',
							'name'  => 'percent',
							'label' => __('Skill Level', SH_NAME),
							'min'   => 1,
							'max'   => 100,
						),
						array(
							'type'  		=> 'textbox',
							'label'       => __('Skill Name', SH_NAME),
							'name'        => 'skill_name',
							'validation'  => '',
						),
						array(
							'type'  		=> 'textbox',
							'label'       => __('Link', SH_NAME),
							'name'        => 'skill_link',
							'validation'  => '',
						),
					),
					
				),
				
	),
);*/

/*$options[] =  array(
	'id'          => 'sh_form_settings',
	'types'       => array( 'sh_forms' ),
	'title'       => __('Form Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 

			array(
					array(
						'type'  => 'radiobutton',
						'name'  => 'form_method',
						'label' => __('Form Submit Method', SH_NAME),
						'items' => array(
							array('value' => 'post', 'label' => __('POST', SH_NAME) ),
							array('value' => 'get', 'label' => __('GET', SH_NAME) ),
						),
						'default' => 'text',
					),
					array(
						'type'  => 'textbox',
						'name'  => 'form_action',
						'label' => __('Form Action URL', SH_NAME),
						'default' => 'text',
						'placeholder' => 'http://example.com'
					),
					array(
						'type'  => 'textbox',
						'name'  => 'form_email',
						'label' => __('Reciever Email', SH_NAME),
						'default' => '',
						'description' => __('Enter the email address of receiver to send the form submission detail', SH_NAME)
					),
					array(
						'type'  => 'textarea',
						'name'  => 'form_success_msg',
						'label' => __('Success Message', SH_NAME),
						'default' => '',
						'description' => __('Enter the message to show after successful submission of the form', SH_NAME)
					),
			)
);*/

/*$options[] =  array(
	'id'          => 'sh_forms_option',
	'types'       => array( 'sh_forms' ),
	'title'       => __('Form Builder', SH_NAME),
	'priority'    => 'high',
	'template'    => 

			array(

				array(
					'type'      => 'group',
					'repeating' => true,
					'sortable'  => true,
					'name'      => 'field',
					'title'     => __('Field', SH_NAME),
					'fields'    => array(
						
						array(
							'type'  => 'select',
							'name'  => 'type',
							'label' => __('Field Type', SH_NAME),
							'items' => array(
								array('value' => 'text', 'label' => __('Text Input', SH_NAME) ),
								array('value' => 'password', 'label' => __('Password', SH_NAME) ),
								array('value' => 'email', 'label' => __('Email', SH_NAME) ),
								array('value' => 'range', 'label' => __('Range', SH_NAME) ),
								array('value' => 'url', 'label' => __('URL', SH_NAME) ),
								array('value' => 'select', 'label' => __('Dropdown', SH_NAME) ),
								array('value' => 'checkbox', 'label' => __('Checkbox', SH_NAME) ),
								array('value' => 'radio', 'label' => __('Radio Button', SH_NAME) ),
								array('value' => 'textarea', 'label' => __('Textarea', SH_NAME) ),
							),
							'default' => 'text',
						),
						array(
							'type'      => 'group',
							'repeating' => true,
							'sortable'  => true,
							'name'      => 'select_value',
							'title'     => __('Dropdown Values', SH_NAME),
							'dependency' => array(
								'field'    => 'type',
								'function' => 'sh_dep_pb_dropdown',
							),
							'fields'    => array(
								
								array(
									'type'  => 'textbox',
									'name'  => 'dropdown_value',
									'label' => __('Value', SH_NAME),
									'default' => 'option1',
								),
								array(
									'type'  => 'textbox',
									'name'  => 'dropdown_label',
									'label' => __('Label', SH_NAME),
									'default' => 'Value1',
								),
							),
						),
						array(
							'type'      => 'group',
							'repeating' => true,
							'sortable'  => true,
							'name'      => 'chechbox_value',
							'title'     => __('Checkbox Values', SH_NAME),
							'dependency' => array(
								'field'    => 'type',
								'function' => 'sh_dep_pb_checkbox',
							),
							'fields'    => array(
								
								array(
									'type'  => 'textbox',
									'name'  => 'check_value',
									'label' => __('Value', SH_NAME),
									'default' => 'option1',
								),
								array(
									'type'  => 'textbox',
									'name'  => 'check_label',
									'label' => __('Label', SH_NAME),
									'default' => 'Value1',
								),
							),
						),
						array(
							'type'      => 'group',
							'repeating' => true,
							'sortable'  => true,
							'name'      => 'radio_btn_value',
							'title'     => __('Radio Values', SH_NAME),
							'dependency' => array(
								'field'    => 'type',
								'function' => 'sh_dep_pb_radio',
							),
							'fields'    => array(
								
								array(
									'type'  => 'textbox',
									'name'  => 'radio_value',
									'label' => __('Value', SH_NAME),
									'default' => 'option1',
								),
								array(
									'type'  => 'textbox',
									'name'  => 'radio_label',
									'label' => __('Label', SH_NAME),
									'default' => 'Value1',
								),
							),
						),
						array(
							'type'  => 'checkbox',
							'name'  => 'validation',
							'label' => __('Validation', SH_NAME),
							'items' => array(
								array('value' => 'required', 'label' => __('Required', SH_NAME) ),
								array('value' => 'alphabet', 'label' => __('Alphabetic', SH_NAME) ),
								array('value' => 'alphanumeric', 'label' => __('Alphanumeric', SH_NAME) ),
								array('value' => 'numeric', 'label' => __('Numeric', SH_NAME) ),
								array('value' => 'email', 'label' => __('Email', SH_NAME) ),
								array('value' => 'url', 'label' => __('URL', SH_NAME) ),
							),
							'default' => 'text',
						),
						array(
							'type'  		=> 'textbox',
							'label'       => __('Field Name', SH_NAME),
							'name'        => 'name',
							'validation'  => '',
						),
						array(
							'type'  		=> 'textbox',
							'label'       => __('Default Value', SH_NAME),
							'name'        => 'default',
							'validation'  => '',
						),
						array(
							'type'  		=> 'textbox',
							'label'       => __('Placeholder', SH_NAME),
							'name'        => 'placeholder',
							'validation'  => '',
						),
						array(
							'type'  		=> 'textbox',
							'label'       => __('Class Attribute', SH_NAME),
							'name'        => 'class',
							'validation'  => '',
						),
						array(
							'type'  		=> 'textbox',
							'label'       => __('id Attribute', SH_NAME),
							'name'        => 'id',
							'validation'  => '',
						),
					),
					
				),
				
	),
);*/


/**
 * EOF
 */
 
 
 return $options;
 
 
 
 
 
 
 
 